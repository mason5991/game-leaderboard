# Game leaderboard

## Prerequisite

Suggest to use Node.js v14.x (This one is developed under Node.js v14.8)

## Installation

```
yarn install
```

## Start application

Open a new terminal to start the server first

```
yarn start:server
```

Then open another terminal to start the web application

```
yarn start
```

Go to URL: (http://localhost:3000)

The server will update the leaderboard every 3 seconds, with random number of records update (max 10 due to number of players)

**Or you can use the build files, which are under build folder. Double click on index.html and open with your browser.**

## Build

```
Build
```

## Test

```
yarn test
```
