const express = require('express');
const app = express();
const http = require('http');
const cors = require('cors');

const randomIntegerWithRange = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

const server = http.createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
    methods: ['GET', 'POST'],
  },
});

app.use(
  cors({
    origin: '*',
  }),
);

app.get('/health', (req, res) => {
  res.send('Hello');
});

io.on('connection', (socket) => {
  console.log('a user connected');

  const sendData = setInterval(() => {
    for (let p = 1; p <= 10; p++) {
      const randomSend = randomIntegerWithRange(1, 10);
      if (randomSend % 2 === 0) {
        const score = randomIntegerWithRange(0, 100);
        const data = {
          name: `player${p}`,
          score: score < 0 || score > 100 ? 100 : score,
          status: randomIntegerWithRange(1, 10) % 2 === 0 ? 'defeat' : 'alive',
        };
        console.log('update', data);
        socket.emit('update-data', data);
      }
    }
  }, 3000);

  socket.on('disconnect', () => {
    clearInterval(sendData);
    console.log('user disconnected');
  });
});

server.listen(3001, () => {
  console.log('listening on *:3001');
});
