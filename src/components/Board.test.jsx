import { render, screen } from '@testing-library/react';
import Board from './Board';

const data = [
  { name: 'player1', score: 0, status: 'alive' },
  { name: 'player2', score: 1, status: 'alive' },
  { name: 'player3', score: 2, status: 'alive' },
  { name: 'player4', score: 3, status: 'alive' },
  { name: 'player5', score: 4, status: 'alive' },
  { name: 'player6', score: 5, status: 'alive' },
  { name: 'player7', score: 6, status: 'alive' },
  { name: 'player8', score: 7, status: 'alive' },
  { name: 'player9', score: 8, status: 'alive' },
  { name: 'player10', score: 9, status: 'alive' },
];
const mapping = [
  {
    label: 'Name',
    target: 'name',
  },
  {
    label: 'Score',
    target: 'score',
  },
  {
    label: 'Status',
    target: 'status',
  },
];

describe('Board', () => {
  test('should render', () => {
    render(<Board data={data} primary="name" orderBy="score" mapping={mapping} />);
    expect(screen.getByText(/Name/i)).toBeTruthy();
  });

  test('should order by asc', () => {
    render(<Board data={data} primary="name" orderBy="score" order="asc" mapping={mapping} />);
    const scoreElements = screen.queryAllByTestId(/board_row_score/i);
    const scores = scoreElements.map((score) => score.textContent);
    expect(scores).toEqual(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']);
  });

  test('should order by desc', () => {
    render(<Board data={data} primary="name" orderBy="score" order="desc" mapping={mapping} />);
    const scoreElements = screen.queryAllByTestId(/board_row_score/i);
    const scores = scoreElements.map((score) => score.textContent);
    expect(scores).toEqual(['9', '8', '7', '6', '5', '4', '3', '2', '1', '0']);
  });
});
