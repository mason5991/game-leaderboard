import PropTypes from 'prop-types';
import _ from 'lodash';
import { Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/react';

const Board = ({ primary, orderBy, order, mapping, data }) => {
  return (
    <Table variant="striped">
      <Thead>
        <Tr>
          {mapping.map((m) => (
            <Th key={`board_header_${m.label}`}>{m.label}</Th>
          ))}
        </Tr>
      </Thead>
      <Tbody>
        {_.orderBy(data, [orderBy], [order]).map((d) => (
          <Tr key={`board_row_${d[primary]}`}>
            {mapping.map((m, index) => (
              <Td data-testid={`board_row_${m.target}`} key={`board_row_${d[primary]}_${m.target}`}>
                {d[m.target]}
              </Td>
            ))}
          </Tr>
        ))}
      </Tbody>
    </Table>
  );
};

Board.propTypes = {
  primary: PropTypes.string.isRequired,
  mapping: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  orderBy: PropTypes.string.isRequired,
  order: PropTypes.string,
};

Board.defaultProps = {
  order: 'desc',
};

export default Board;
