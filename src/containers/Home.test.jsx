import { render, screen } from '@testing-library/react';
import socketIOClient from 'socket.io-client';
import MockedSocket from 'socket.io-mock';
import { Provider } from 'react-redux';
import Home from './Home';
import { configureStore } from '@reduxjs/toolkit';
import leaderboardReducer from '../redux/slices/leaderboard.slice';

jest.mock('socket.io-client');

describe('Home', () => {
  function Wrapper({ children }) {
    const store = configureStore({ reducer: { leaderboard: leaderboardReducer } });
    return <Provider store={store}>{children}</Provider>;
  }
  let socket;

  beforeEach(() => {
    socket = new MockedSocket();
    socketIOClient.mockReturnValue(socket);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  test('should render and connect socket', () => {
    render(
      <Wrapper>
        <Home />
      </Wrapper>,
    );
    expect(socketIOClient.connect).toHaveBeenCalled();
    expect(screen.getByText(/game leaderboard/i)).toBeTruthy();
    expect(screen.getByText('player1')).toBeTruthy();
  });
});
