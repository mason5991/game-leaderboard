import { useEffect } from 'react';
import io from 'socket.io-client';
import { updateData } from '../redux/slices/leaderboard.slice';
import { useSelector, useDispatch } from '../redux';
import Board from '../components/Board';
import { Container, Box, Heading } from '@chakra-ui/react';

const Home = () => {
  const mapping = [
    {
      label: 'Name',
      target: 'name',
    },
    {
      label: 'Score',
      target: 'score',
    },
    {
      label: 'Status',
      target: 'status',
    },
  ];
  const { data } = useSelector((state) => state.leaderboard);
  const dispatch = useDispatch();

  const updateBoardData = (data) => {
    dispatch(updateData(data));
  };

  useEffect(() => {
    const socket = io('http://localhost:3001');
    socket.on('connect', () => {
      console.log('connected');
      socket.on('update-data', (data) => {
        updateBoardData(data);
      });
      socket.on('disconnect', () => {
        console.log('disconnected');
      });
    });

    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <Container>
      <Box py={4}>
        <Heading size="md" color="gray.500">
          Game leaderboard
        </Heading>
      </Box>
      <Board primary="name" mapping={mapping} data={data} orderBy="score" />
    </Container>
  );
};

export default Home;
