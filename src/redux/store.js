import { configureStore } from '@reduxjs/toolkit';
import leaderboard from './slices/leaderboard.slice';

export const store = configureStore({
  reducer: {
    leaderboard,
  },
});
