import reducer, { updateData } from './leaderboard.slice';

const defaultState = {
  data: [
    { name: 'player1', score: 0, status: 'alive' },
    { name: 'player2', score: 0, status: 'alive' },
    { name: 'player3', score: 0, status: 'alive' },
    { name: 'player4', score: 0, status: 'alive' },
    { name: 'player5', score: 0, status: 'alive' },
    { name: 'player6', score: 0, status: 'alive' },
    { name: 'player7', score: 0, status: 'alive' },
    { name: 'player8', score: 0, status: 'alive' },
    { name: 'player9', score: 0, status: 'alive' },
    { name: 'player10', score: 0, status: 'alive' },
  ],
};

describe('leaderboard slice', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(defaultState);
  });

  test('should update data', () => {
    expect(reducer(defaultState, updateData({ name: 'player1', score: 100, status: 'defeated' }))).toEqual({
      data: [
        { name: 'player1', score: 100, status: 'defeated' },
        { name: 'player2', score: 0, status: 'alive' },
        { name: 'player3', score: 0, status: 'alive' },
        { name: 'player4', score: 0, status: 'alive' },
        { name: 'player5', score: 0, status: 'alive' },
        { name: 'player6', score: 0, status: 'alive' },
        { name: 'player7', score: 0, status: 'alive' },
        { name: 'player8', score: 0, status: 'alive' },
        { name: 'player9', score: 0, status: 'alive' },
        { name: 'player10', score: 0, status: 'alive' },
      ],
    });
  });
});
