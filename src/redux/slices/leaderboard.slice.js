import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: [
    { name: 'player1', score: 0, status: 'alive' },
    { name: 'player2', score: 0, status: 'alive' },
    { name: 'player3', score: 0, status: 'alive' },
    { name: 'player4', score: 0, status: 'alive' },
    { name: 'player5', score: 0, status: 'alive' },
    { name: 'player6', score: 0, status: 'alive' },
    { name: 'player7', score: 0, status: 'alive' },
    { name: 'player8', score: 0, status: 'alive' },
    { name: 'player9', score: 0, status: 'alive' },
    { name: 'player10', score: 0, status: 'alive' },
  ],
};

export const leaderboardSlice = createSlice({
  name: 'leaderboard',
  initialState,
  reducers: {
    updateData: (state, action) => {
      state.data = [...state.data].map((d) => (d.name === action.payload.name ? action.payload : d));
    },
  },
});

export const { updateData } = leaderboardSlice.actions;

export default leaderboardSlice.reducer;
